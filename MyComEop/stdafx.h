// stdafx.h : 标准系统包含文件的包含文件，
// 或是经常使用但不常更改的
// 特定于项目的包含文件
//

#ifdef ARRAYSIZE
#define countof(a) (ARRAYSIZE(a)) // (sizeof((a))/(sizeof(*(a))))
#else
#define countof(a) (sizeof((a)) / (sizeof(*(a))))

#endif






#pragma once

#include "targetver.h"
#include <bits.h>
#include <bits4_0.h>
#include <tchar.h>
#include <string>
#include <vector>
#include <strsafe.h>
#include <comdef.h>
#include <Sddl.h>
#include <atlbase.h>
#include <AclAPI.h>
#include <shellapi.h>
#include "../MyComDefine/taskschedule_h.h"
#include "../MyComDefine/objectexporter_h.h"
#include "../MyComDefine/winspool_h.h"
#define LEN 1024

bool CreateNativeHardlink(LPCWSTR linkname, LPCWSTR targetname);



// TODO:  在此处引用程序需要的其他头文件

/*
typedef LUID OXID;
typedef LUID OID;
typedef GUID	IPID;*/

typedef struct tagDUALSTRINGARRAY2    {
	unsigned short wNumEntries;     // Number of entries in array.
	unsigned short wSecurityOffset; // Offset of security info.
	unsigned short aStringArray[];
} DUALSTRINGARRAY2;

typedef struct tagSTDOBJREF2    {
	DWORD   flags;
	DWORD   cPublicRefs;
	OXID           oxid;
	OID            oid;
	IPID           ipid;
} STDOBJREF2;

typedef struct tagOBJREF2    {
	unsigned long signature;//MEOW
	unsigned long flags;
	GUID          iid;
	union        {
		struct            {
			STDOBJREF       std;
			DUALSTRINGARRAY saResAddr;
		} u_standard;
		struct            {
			STDOBJREF       std;
			CLSID           clsid;
			DUALSTRINGARRAY saResAddr;
		} u_handler;
		struct            {
			CLSID           clsid;
			unsigned long   cbExtension;
			unsigned long   size;
			ULONGLONG pData;
		} u_custom;
	} u_objref;
} OBJREF2;

typedef struct ExploitData{
	;

	IID IID_Interface;
	IID TypeLib_Interface;
	OLECHAR extPath[LEN];
	OLECHAR dllPath[LEN];
	OLECHAR Interface_Name[LEN];
	UINT32 InterfaceVersionFirst;
	UINT32 InterfaceVersionNext;
}*PExploitData;