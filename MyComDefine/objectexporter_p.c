

/* this ALWAYS GENERATED file contains the proxy stub code */


 /* File created by MIDL compiler version 8.00.0603 */
/* at Wed May 06 17:26:14 2020
 */
/* Compiler settings for objectexporter.idl:
    Oicf, W1, Zp8, env=Win32 (32b run), target_arch=X86 8.00.0603 
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#if !defined(_M_IA64) && !defined(_M_AMD64) && !defined(_ARM_)


#pragma warning( disable: 4049 )  /* more than 64k source lines */
#if _MSC_VER >= 1200
#pragma warning(push)
#endif

#pragma warning( disable: 4211 )  /* redefine extern to static */
#pragma warning( disable: 4232 )  /* dllimport identity*/
#pragma warning( disable: 4024 )  /* array to pointer mapping*/
#pragma warning( disable: 4152 )  /* function/data pointer conversion in expression */
#pragma warning( disable: 4100 ) /* unreferenced arguments in x86 call */

#pragma optimize("", off ) 

#define USE_STUBLESS_PROXY


/* verify that the <rpcproxy.h> version is high enough to compile this file*/
#ifndef __REDQ_RPCPROXY_H_VERSION__
#define __REQUIRED_RPCPROXY_H_VERSION__ 475
#endif


#include "rpcproxy.h"
#ifndef __RPCPROXY_H_VERSION__
#error this stub requires an updated version of <rpcproxy.h>
#endif /* __RPCPROXY_H_VERSION__ */


#include "objectexporter_h.h"

#define TYPE_FORMAT_STRING_SIZE   589                               
#define PROC_FORMAT_STRING_SIZE   913                               
#define EXPR_FORMAT_STRING_SIZE   1                                 
#define TRANSMIT_AS_TABLE_SIZE    0            
#define WIRE_MARSHAL_TABLE_SIZE   0            

typedef struct _objectexporter_MIDL_TYPE_FORMAT_STRING
    {
    short          Pad;
    unsigned char  Format[ TYPE_FORMAT_STRING_SIZE ];
    } objectexporter_MIDL_TYPE_FORMAT_STRING;

typedef struct _objectexporter_MIDL_PROC_FORMAT_STRING
    {
    short          Pad;
    unsigned char  Format[ PROC_FORMAT_STRING_SIZE ];
    } objectexporter_MIDL_PROC_FORMAT_STRING;

typedef struct _objectexporter_MIDL_EXPR_FORMAT_STRING
    {
    long          Pad;
    unsigned char  Format[ EXPR_FORMAT_STRING_SIZE ];
    } objectexporter_MIDL_EXPR_FORMAT_STRING;


static const RPC_SYNTAX_IDENTIFIER  _RpcTransferSyntax = 
{{0x8A885D04,0x1CEB,0x11C9,{0x9F,0xE8,0x08,0x00,0x2B,0x10,0x48,0x60}},{2,0}};


extern const objectexporter_MIDL_TYPE_FORMAT_STRING objectexporter__MIDL_TypeFormatString;
extern const objectexporter_MIDL_PROC_FORMAT_STRING objectexporter__MIDL_ProcFormatString;
extern const objectexporter_MIDL_EXPR_FORMAT_STRING objectexporter__MIDL_ExprFormatString;


extern const MIDL_STUB_DESC Object_StubDesc;


extern const MIDL_SERVER_INFO IRemUnknown_ServerInfo;
extern const MIDL_STUBLESS_PROXY_INFO IRemUnknown_ProxyInfo;


extern const MIDL_STUB_DESC Object_StubDesc;


extern const MIDL_SERVER_INFO IRemUnknown2_ServerInfo;
extern const MIDL_STUBLESS_PROXY_INFO IRemUnknown2_ProxyInfo;


extern const EXPR_EVAL ExprEvalRoutines[];

#if !defined(__RPC_WIN32__)
#error  Invalid build platform for this stub.
#endif

#if !(TARGET_IS_NT50_OR_LATER)
#error You need Windows 2000 or later to run this stub because it uses these features:
#error   /robust command line switch.
#error However, your C/C++ compilation flags indicate you intend to run this app on earlier systems.
#error This app will fail with the RPC_X_WRONG_STUB_VERSION error.
#endif


static const objectexporter_MIDL_PROC_FORMAT_STRING objectexporter__MIDL_ProcFormatString =
    {
        0,
        {

	/* Procedure ResolveOxid */

			0x0,		/* 0 */
			0x48,		/* Old Flags:  */
/*  2 */	NdrFcLong( 0x1 ),	/* 1 */
/*  6 */	NdrFcShort( 0x0 ),	/* 0 */
/*  8 */	NdrFcShort( 0x20 ),	/* x86 Stack size/offset = 32 */
/* 10 */	0x32,		/* FC_BIND_PRIMITIVE */
			0x0,		/* 0 */
/* 12 */	NdrFcShort( 0x0 ),	/* x86 Stack size/offset = 0 */
/* 14 */	NdrFcShort( 0x2a ),	/* 42 */
/* 16 */	NdrFcShort( 0x68 ),	/* 104 */
/* 18 */	0x47,		/* Oi2 Flags:  srv must size, clt must size, has return, has ext, */
			0x7,		/* 7 */
/* 20 */	0x8,		/* 8 */
			0x7,		/* Ext Flags:  new corr desc, clt corr check, srv corr check, */
/* 22 */	NdrFcShort( 0x1 ),	/* 1 */
/* 24 */	NdrFcShort( 0x1 ),	/* 1 */
/* 26 */	NdrFcShort( 0x0 ),	/* 0 */

	/* Parameter hRpc */

/* 28 */	NdrFcShort( 0x148 ),	/* Flags:  in, base type, simple ref, */
/* 30 */	NdrFcShort( 0x4 ),	/* x86 Stack size/offset = 4 */
/* 32 */	0xb,		/* FC_HYPER */
			0x0,		/* 0 */

	/* Parameter pOxid */

/* 34 */	NdrFcShort( 0x48 ),	/* Flags:  in, base type, */
/* 36 */	NdrFcShort( 0x8 ),	/* x86 Stack size/offset = 8 */
/* 38 */	0x6,		/* FC_SHORT */
			0x0,		/* 0 */

	/* Parameter cRequestedProtseqs */

/* 40 */	NdrFcShort( 0xb ),	/* Flags:  must size, must free, in, */
/* 42 */	NdrFcShort( 0xc ),	/* x86 Stack size/offset = 12 */
/* 44 */	NdrFcShort( 0x6 ),	/* Type Offset=6 */

	/* Parameter arRequestedProtseqs */

/* 46 */	NdrFcShort( 0x2013 ),	/* Flags:  must size, must free, out, srv alloc size=8 */
/* 48 */	NdrFcShort( 0x10 ),	/* x86 Stack size/offset = 16 */
/* 50 */	NdrFcShort( 0x12 ),	/* Type Offset=18 */

	/* Parameter ppdsaOxidBindings */

/* 52 */	NdrFcShort( 0x4112 ),	/* Flags:  must free, out, simple ref, srv alloc size=16 */
/* 54 */	NdrFcShort( 0x14 ),	/* x86 Stack size/offset = 20 */
/* 56 */	NdrFcShort( 0x3a ),	/* Type Offset=58 */

	/* Parameter pipidRemUnknown */

/* 58 */	NdrFcShort( 0x2150 ),	/* Flags:  out, base type, simple ref, srv alloc size=8 */
/* 60 */	NdrFcShort( 0x18 ),	/* x86 Stack size/offset = 24 */
/* 62 */	0x8,		/* FC_LONG */
			0x0,		/* 0 */

	/* Parameter pAuthnHint */

/* 64 */	NdrFcShort( 0x70 ),	/* Flags:  out, return, base type, */
/* 66 */	NdrFcShort( 0x1c ),	/* x86 Stack size/offset = 28 */
/* 68 */	0x10,		/* FC_ERROR_STATUS_T */
			0x0,		/* 0 */

	/* Procedure SimplePing */


	/* Return value */

/* 70 */	0x0,		/* 0 */
			0x48,		/* Old Flags:  */
/* 72 */	NdrFcLong( 0x1 ),	/* 1 */
/* 76 */	NdrFcShort( 0x1 ),	/* 1 */
/* 78 */	NdrFcShort( 0xc ),	/* x86 Stack size/offset = 12 */
/* 80 */	0x32,		/* FC_BIND_PRIMITIVE */
			0x0,		/* 0 */
/* 82 */	NdrFcShort( 0x0 ),	/* x86 Stack size/offset = 0 */
/* 84 */	NdrFcShort( 0x24 ),	/* 36 */
/* 86 */	NdrFcShort( 0x8 ),	/* 8 */
/* 88 */	0x44,		/* Oi2 Flags:  has return, has ext, */
			0x2,		/* 2 */
/* 90 */	0x8,		/* 8 */
			0x1,		/* Ext Flags:  new corr desc, */
/* 92 */	NdrFcShort( 0x0 ),	/* 0 */
/* 94 */	NdrFcShort( 0x0 ),	/* 0 */
/* 96 */	NdrFcShort( 0x0 ),	/* 0 */

	/* Parameter hRpc */

/* 98 */	NdrFcShort( 0x148 ),	/* Flags:  in, base type, simple ref, */
/* 100 */	NdrFcShort( 0x4 ),	/* x86 Stack size/offset = 4 */
/* 102 */	0xb,		/* FC_HYPER */
			0x0,		/* 0 */

	/* Parameter pSetId */

/* 104 */	NdrFcShort( 0x70 ),	/* Flags:  out, return, base type, */
/* 106 */	NdrFcShort( 0x8 ),	/* x86 Stack size/offset = 8 */
/* 108 */	0x10,		/* FC_ERROR_STATUS_T */
			0x0,		/* 0 */

	/* Procedure ComplexPing */


	/* Return value */

/* 110 */	0x0,		/* 0 */
			0x48,		/* Old Flags:  */
/* 112 */	NdrFcLong( 0x1 ),	/* 1 */
/* 116 */	NdrFcShort( 0x2 ),	/* 2 */
/* 118 */	NdrFcShort( 0x24 ),	/* x86 Stack size/offset = 36 */
/* 120 */	0x32,		/* FC_BIND_PRIMITIVE */
			0x0,		/* 0 */
/* 122 */	NdrFcShort( 0x0 ),	/* x86 Stack size/offset = 0 */
/* 124 */	NdrFcShort( 0x36 ),	/* 54 */
/* 126 */	NdrFcShort( 0x46 ),	/* 70 */
/* 128 */	0x46,		/* Oi2 Flags:  clt must size, has return, has ext, */
			0x8,		/* 8 */
/* 130 */	0x8,		/* 8 */
			0x5,		/* Ext Flags:  new corr desc, srv corr check, */
/* 132 */	NdrFcShort( 0x0 ),	/* 0 */
/* 134 */	NdrFcShort( 0x1 ),	/* 1 */
/* 136 */	NdrFcShort( 0x0 ),	/* 0 */

	/* Parameter hRpc */

/* 138 */	NdrFcShort( 0x158 ),	/* Flags:  in, out, base type, simple ref, */
/* 140 */	NdrFcShort( 0x4 ),	/* x86 Stack size/offset = 4 */
/* 142 */	0xb,		/* FC_HYPER */
			0x0,		/* 0 */

	/* Parameter pSetId */

/* 144 */	NdrFcShort( 0x48 ),	/* Flags:  in, base type, */
/* 146 */	NdrFcShort( 0x8 ),	/* x86 Stack size/offset = 8 */
/* 148 */	0x6,		/* FC_SHORT */
			0x0,		/* 0 */

	/* Parameter SequenceNum */

/* 150 */	NdrFcShort( 0x48 ),	/* Flags:  in, base type, */
/* 152 */	NdrFcShort( 0xc ),	/* x86 Stack size/offset = 12 */
/* 154 */	0x6,		/* FC_SHORT */
			0x0,		/* 0 */

	/* Parameter cAddToSet */

/* 156 */	NdrFcShort( 0x48 ),	/* Flags:  in, base type, */
/* 158 */	NdrFcShort( 0x10 ),	/* x86 Stack size/offset = 16 */
/* 160 */	0x6,		/* FC_SHORT */
			0x0,		/* 0 */

	/* Parameter cDelFromSet */

/* 162 */	NdrFcShort( 0xb ),	/* Flags:  must size, must free, in, */
/* 164 */	NdrFcShort( 0x14 ),	/* x86 Stack size/offset = 20 */
/* 166 */	NdrFcShort( 0x4a ),	/* Type Offset=74 */

	/* Parameter AddToSet */

/* 168 */	NdrFcShort( 0xb ),	/* Flags:  must size, must free, in, */
/* 170 */	NdrFcShort( 0x18 ),	/* x86 Stack size/offset = 24 */
/* 172 */	NdrFcShort( 0x5a ),	/* Type Offset=90 */

	/* Parameter DelFromSet */

/* 174 */	NdrFcShort( 0x2150 ),	/* Flags:  out, base type, simple ref, srv alloc size=8 */
/* 176 */	NdrFcShort( 0x1c ),	/* x86 Stack size/offset = 28 */
/* 178 */	0x6,		/* FC_SHORT */
			0x0,		/* 0 */

	/* Parameter pPingBackoffFactor */

/* 180 */	NdrFcShort( 0x70 ),	/* Flags:  out, return, base type, */
/* 182 */	NdrFcShort( 0x20 ),	/* x86 Stack size/offset = 32 */
/* 184 */	0x10,		/* FC_ERROR_STATUS_T */
			0x0,		/* 0 */

	/* Procedure ServerAlive */


	/* Return value */

/* 186 */	0x0,		/* 0 */
			0x48,		/* Old Flags:  */
/* 188 */	NdrFcLong( 0x1 ),	/* 1 */
/* 192 */	NdrFcShort( 0x3 ),	/* 3 */
/* 194 */	NdrFcShort( 0x8 ),	/* x86 Stack size/offset = 8 */
/* 196 */	0x32,		/* FC_BIND_PRIMITIVE */
			0x0,		/* 0 */
/* 198 */	NdrFcShort( 0x0 ),	/* x86 Stack size/offset = 0 */
/* 200 */	NdrFcShort( 0x0 ),	/* 0 */
/* 202 */	NdrFcShort( 0x8 ),	/* 8 */
/* 204 */	0x44,		/* Oi2 Flags:  has return, has ext, */
			0x1,		/* 1 */
/* 206 */	0x8,		/* 8 */
			0x1,		/* Ext Flags:  new corr desc, */
/* 208 */	NdrFcShort( 0x0 ),	/* 0 */
/* 210 */	NdrFcShort( 0x0 ),	/* 0 */
/* 212 */	NdrFcShort( 0x0 ),	/* 0 */

	/* Parameter hRpc */

/* 214 */	NdrFcShort( 0x70 ),	/* Flags:  out, return, base type, */
/* 216 */	NdrFcShort( 0x4 ),	/* x86 Stack size/offset = 4 */
/* 218 */	0x10,		/* FC_ERROR_STATUS_T */
			0x0,		/* 0 */

	/* Procedure ResolveOxid2 */


	/* Return value */

/* 220 */	0x0,		/* 0 */
			0x48,		/* Old Flags:  */
/* 222 */	NdrFcLong( 0x1 ),	/* 1 */
/* 226 */	NdrFcShort( 0x4 ),	/* 4 */
/* 228 */	NdrFcShort( 0x24 ),	/* x86 Stack size/offset = 36 */
/* 230 */	0x32,		/* FC_BIND_PRIMITIVE */
			0x0,		/* 0 */
/* 232 */	NdrFcShort( 0x0 ),	/* x86 Stack size/offset = 0 */
/* 234 */	NdrFcShort( 0x2a ),	/* 42 */
/* 236 */	NdrFcShort( 0x90 ),	/* 144 */
/* 238 */	0x47,		/* Oi2 Flags:  srv must size, clt must size, has return, has ext, */
			0x8,		/* 8 */
/* 240 */	0x8,		/* 8 */
			0x7,		/* Ext Flags:  new corr desc, clt corr check, srv corr check, */
/* 242 */	NdrFcShort( 0x1 ),	/* 1 */
/* 244 */	NdrFcShort( 0x1 ),	/* 1 */
/* 246 */	NdrFcShort( 0x0 ),	/* 0 */

	/* Parameter hRpc */

/* 248 */	NdrFcShort( 0x148 ),	/* Flags:  in, base type, simple ref, */
/* 250 */	NdrFcShort( 0x4 ),	/* x86 Stack size/offset = 4 */
/* 252 */	0xb,		/* FC_HYPER */
			0x0,		/* 0 */

	/* Parameter pOxid */

/* 254 */	NdrFcShort( 0x48 ),	/* Flags:  in, base type, */
/* 256 */	NdrFcShort( 0x8 ),	/* x86 Stack size/offset = 8 */
/* 258 */	0x6,		/* FC_SHORT */
			0x0,		/* 0 */

	/* Parameter cRequestedProtseqs */

/* 260 */	NdrFcShort( 0xb ),	/* Flags:  must size, must free, in, */
/* 262 */	NdrFcShort( 0xc ),	/* x86 Stack size/offset = 12 */
/* 264 */	NdrFcShort( 0x6 ),	/* Type Offset=6 */

	/* Parameter arRequestedProtseqs */

/* 266 */	NdrFcShort( 0x2013 ),	/* Flags:  must size, must free, out, srv alloc size=8 */
/* 268 */	NdrFcShort( 0x10 ),	/* x86 Stack size/offset = 16 */
/* 270 */	NdrFcShort( 0x12 ),	/* Type Offset=18 */

	/* Parameter ppdsaOxidBindings */

/* 272 */	NdrFcShort( 0x4112 ),	/* Flags:  must free, out, simple ref, srv alloc size=16 */
/* 274 */	NdrFcShort( 0x14 ),	/* x86 Stack size/offset = 20 */
/* 276 */	NdrFcShort( 0x3a ),	/* Type Offset=58 */

	/* Parameter pipidRemUnknown */

/* 278 */	NdrFcShort( 0x2150 ),	/* Flags:  out, base type, simple ref, srv alloc size=8 */
/* 280 */	NdrFcShort( 0x18 ),	/* x86 Stack size/offset = 24 */
/* 282 */	0x8,		/* FC_LONG */
			0x0,		/* 0 */

	/* Parameter pAuthnHint */

/* 284 */	NdrFcShort( 0x2112 ),	/* Flags:  must free, out, simple ref, srv alloc size=8 */
/* 286 */	NdrFcShort( 0x1c ),	/* x86 Stack size/offset = 28 */
/* 288 */	NdrFcShort( 0x72 ),	/* Type Offset=114 */

	/* Parameter pComVersion */

/* 290 */	NdrFcShort( 0x70 ),	/* Flags:  out, return, base type, */
/* 292 */	NdrFcShort( 0x20 ),	/* x86 Stack size/offset = 32 */
/* 294 */	0x10,		/* FC_ERROR_STATUS_T */
			0x0,		/* 0 */

	/* Procedure ServerAlive2 */


	/* Return value */

/* 296 */	0x0,		/* 0 */
			0x48,		/* Old Flags:  */
/* 298 */	NdrFcLong( 0x1 ),	/* 1 */
/* 302 */	NdrFcShort( 0x5 ),	/* 5 */
/* 304 */	NdrFcShort( 0x14 ),	/* x86 Stack size/offset = 20 */
/* 306 */	0x32,		/* FC_BIND_PRIMITIVE */
			0x0,		/* 0 */
/* 308 */	NdrFcShort( 0x0 ),	/* x86 Stack size/offset = 0 */
/* 310 */	NdrFcShort( 0x0 ),	/* 0 */
/* 312 */	NdrFcShort( 0x4c ),	/* 76 */
/* 314 */	0x45,		/* Oi2 Flags:  srv must size, has return, has ext, */
			0x4,		/* 4 */
/* 316 */	0x8,		/* 8 */
			0x3,		/* Ext Flags:  new corr desc, clt corr check, */
/* 318 */	NdrFcShort( 0x1 ),	/* 1 */
/* 320 */	NdrFcShort( 0x0 ),	/* 0 */
/* 322 */	NdrFcShort( 0x0 ),	/* 0 */

	/* Parameter hRpc */

/* 324 */	NdrFcShort( 0x2112 ),	/* Flags:  must free, out, simple ref, srv alloc size=8 */
/* 326 */	NdrFcShort( 0x4 ),	/* x86 Stack size/offset = 4 */
/* 328 */	NdrFcShort( 0x72 ),	/* Type Offset=114 */

	/* Parameter pComVersion */

/* 330 */	NdrFcShort( 0x2013 ),	/* Flags:  must size, must free, out, srv alloc size=8 */
/* 332 */	NdrFcShort( 0x8 ),	/* x86 Stack size/offset = 8 */
/* 334 */	NdrFcShort( 0x12 ),	/* Type Offset=18 */

	/* Parameter ppdsaOrBindings */

/* 336 */	NdrFcShort( 0x2150 ),	/* Flags:  out, base type, simple ref, srv alloc size=8 */
/* 338 */	NdrFcShort( 0xc ),	/* x86 Stack size/offset = 12 */
/* 340 */	0x8,		/* FC_LONG */
			0x0,		/* 0 */

	/* Parameter pReserved */

/* 342 */	NdrFcShort( 0x70 ),	/* Flags:  out, return, base type, */
/* 344 */	NdrFcShort( 0x10 ),	/* x86 Stack size/offset = 16 */
/* 346 */	0x10,		/* FC_ERROR_STATUS_T */
			0x0,		/* 0 */

	/* Procedure RemoteActivation */


	/* Return value */

/* 348 */	0x0,		/* 0 */
			0x48,		/* Old Flags:  */
/* 350 */	NdrFcLong( 0x0 ),	/* 0 */
/* 354 */	NdrFcShort( 0x0 ),	/* 0 */
/* 356 */	NdrFcShort( 0x54 ),	/* x86 Stack size/offset = 84 */
/* 358 */	0x32,		/* FC_BIND_PRIMITIVE */
			0x0,		/* 0 */
/* 360 */	NdrFcShort( 0x0 ),	/* x86 Stack size/offset = 0 */
/* 362 */	NdrFcShort( 0x62 ),	/* 98 */
/* 364 */	NdrFcShort( 0xd0 ),	/* 208 */
/* 366 */	0x47,		/* Oi2 Flags:  srv must size, clt must size, has return, has ext, */
			0x14,		/* 20 */
/* 368 */	0x8,		/* 8 */
			0x7,		/* Ext Flags:  new corr desc, clt corr check, srv corr check, */
/* 370 */	NdrFcShort( 0x1 ),	/* 1 */
/* 372 */	NdrFcShort( 0x1 ),	/* 1 */
/* 374 */	NdrFcShort( 0x0 ),	/* 0 */

	/* Parameter hRpc */

/* 376 */	NdrFcShort( 0x10b ),	/* Flags:  must size, must free, in, simple ref, */
/* 378 */	NdrFcShort( 0x4 ),	/* x86 Stack size/offset = 4 */
/* 380 */	NdrFcShort( 0xcc ),	/* Type Offset=204 */

	/* Parameter ORPCthis */

/* 382 */	NdrFcShort( 0x2113 ),	/* Flags:  must size, must free, out, simple ref, srv alloc size=8 */
/* 384 */	NdrFcShort( 0x8 ),	/* x86 Stack size/offset = 8 */
/* 386 */	NdrFcShort( 0xee ),	/* Type Offset=238 */

	/* Parameter ORPCthat */

/* 388 */	NdrFcShort( 0x10a ),	/* Flags:  must free, in, simple ref, */
/* 390 */	NdrFcShort( 0xc ),	/* x86 Stack size/offset = 12 */
/* 392 */	NdrFcShort( 0x3a ),	/* Type Offset=58 */

	/* Parameter Clsid */

/* 394 */	NdrFcShort( 0xb ),	/* Flags:  must size, must free, in, */
/* 396 */	NdrFcShort( 0x10 ),	/* x86 Stack size/offset = 16 */
/* 398 */	NdrFcShort( 0x106 ),	/* Type Offset=262 */

	/* Parameter pwszObjectName */

/* 400 */	NdrFcShort( 0xb ),	/* Flags:  must size, must free, in, */
/* 402 */	NdrFcShort( 0x14 ),	/* x86 Stack size/offset = 20 */
/* 404 */	NdrFcShort( 0x10a ),	/* Type Offset=266 */

	/* Parameter pObjectStorage */

/* 406 */	NdrFcShort( 0x48 ),	/* Flags:  in, base type, */
/* 408 */	NdrFcShort( 0x18 ),	/* x86 Stack size/offset = 24 */
/* 410 */	0x8,		/* FC_LONG */
			0x0,		/* 0 */

	/* Parameter ClientImpLevel */

/* 412 */	NdrFcShort( 0x48 ),	/* Flags:  in, base type, */
/* 414 */	NdrFcShort( 0x1c ),	/* x86 Stack size/offset = 28 */
/* 416 */	0x8,		/* FC_LONG */
			0x0,		/* 0 */

	/* Parameter Mode */

/* 418 */	NdrFcShort( 0x88 ),	/* Flags:  in, by val, */
/* 420 */	NdrFcShort( 0x20 ),	/* x86 Stack size/offset = 32 */
/* 422 */	NdrFcShort( 0x122 ),	/* 290 */

	/* Parameter Interfaces */

/* 424 */	NdrFcShort( 0xb ),	/* Flags:  must size, must free, in, */
/* 426 */	NdrFcShort( 0x24 ),	/* x86 Stack size/offset = 36 */
/* 428 */	NdrFcShort( 0x12c ),	/* Type Offset=300 */

	/* Parameter pIIDs */

/* 430 */	NdrFcShort( 0x88 ),	/* Flags:  in, by val, */
/* 432 */	NdrFcShort( 0x28 ),	/* x86 Stack size/offset = 40 */
/* 434 */	NdrFcShort( 0x140 ),	/* 320 */

	/* Parameter cRequestedProtseqs */

/* 436 */	NdrFcShort( 0xb ),	/* Flags:  must size, must free, in, */
/* 438 */	NdrFcShort( 0x2c ),	/* x86 Stack size/offset = 44 */
/* 440 */	NdrFcShort( 0x14a ),	/* Type Offset=330 */

	/* Parameter aRequestedProtseqs */

/* 442 */	NdrFcShort( 0x2150 ),	/* Flags:  out, base type, simple ref, srv alloc size=8 */
/* 444 */	NdrFcShort( 0x30 ),	/* x86 Stack size/offset = 48 */
/* 446 */	0xb,		/* FC_HYPER */
			0x0,		/* 0 */

	/* Parameter pOxid */

/* 448 */	NdrFcShort( 0x2013 ),	/* Flags:  must size, must free, out, srv alloc size=8 */
/* 450 */	NdrFcShort( 0x34 ),	/* x86 Stack size/offset = 52 */
/* 452 */	NdrFcShort( 0x12 ),	/* Type Offset=18 */

	/* Parameter ppdsaOxidBindings */

/* 454 */	NdrFcShort( 0x4112 ),	/* Flags:  must free, out, simple ref, srv alloc size=16 */
/* 456 */	NdrFcShort( 0x38 ),	/* x86 Stack size/offset = 56 */
/* 458 */	NdrFcShort( 0x3a ),	/* Type Offset=58 */

	/* Parameter pipidRemUnknown */

/* 460 */	NdrFcShort( 0x2150 ),	/* Flags:  out, base type, simple ref, srv alloc size=8 */
/* 462 */	NdrFcShort( 0x3c ),	/* x86 Stack size/offset = 60 */
/* 464 */	0x8,		/* FC_LONG */
			0x0,		/* 0 */

	/* Parameter pAuthnHint */

/* 466 */	NdrFcShort( 0x2112 ),	/* Flags:  must free, out, simple ref, srv alloc size=8 */
/* 468 */	NdrFcShort( 0x40 ),	/* x86 Stack size/offset = 64 */
/* 470 */	NdrFcShort( 0x72 ),	/* Type Offset=114 */

	/* Parameter pServerVersion */

/* 472 */	NdrFcShort( 0x2150 ),	/* Flags:  out, base type, simple ref, srv alloc size=8 */
/* 474 */	NdrFcShort( 0x44 ),	/* x86 Stack size/offset = 68 */
/* 476 */	0x8,		/* FC_LONG */
			0x0,		/* 0 */

	/* Parameter phr */

/* 478 */	NdrFcShort( 0x113 ),	/* Flags:  must size, must free, out, simple ref, */
/* 480 */	NdrFcShort( 0x48 ),	/* x86 Stack size/offset = 72 */
/* 482 */	NdrFcShort( 0x15e ),	/* Type Offset=350 */

	/* Parameter ppInterfaceData */

/* 484 */	NdrFcShort( 0x113 ),	/* Flags:  must size, must free, out, simple ref, */
/* 486 */	NdrFcShort( 0x4c ),	/* x86 Stack size/offset = 76 */
/* 488 */	NdrFcShort( 0x182 ),	/* Type Offset=386 */

	/* Parameter pResults */

/* 490 */	NdrFcShort( 0x70 ),	/* Flags:  out, return, base type, */
/* 492 */	NdrFcShort( 0x50 ),	/* x86 Stack size/offset = 80 */
/* 494 */	0x10,		/* FC_ERROR_STATUS_T */
			0x0,		/* 0 */

	/* Procedure Opnum0NotUsedOnWire */


	/* Return value */

/* 496 */	0x0,		/* 0 */
			0x48,		/* Old Flags:  */
/* 498 */	NdrFcLong( 0x0 ),	/* 0 */
/* 502 */	NdrFcShort( 0x0 ),	/* 0 */
/* 504 */	NdrFcShort( 0x4 ),	/* x86 Stack size/offset = 4 */
/* 506 */	0x32,		/* FC_BIND_PRIMITIVE */
			0x0,		/* 0 */
/* 508 */	NdrFcShort( 0x0 ),	/* x86 Stack size/offset = 0 */
/* 510 */	NdrFcShort( 0x0 ),	/* 0 */
/* 512 */	NdrFcShort( 0x0 ),	/* 0 */
/* 514 */	0x40,		/* Oi2 Flags:  has ext, */
			0x0,		/* 0 */
/* 516 */	0x8,		/* 8 */
			0x1,		/* Ext Flags:  new corr desc, */
/* 518 */	NdrFcShort( 0x0 ),	/* 0 */
/* 520 */	NdrFcShort( 0x0 ),	/* 0 */
/* 522 */	NdrFcShort( 0x0 ),	/* 0 */

	/* Procedure Opnum1NotUsedOnWire */


	/* Parameter rpc */

/* 524 */	0x0,		/* 0 */
			0x48,		/* Old Flags:  */
/* 526 */	NdrFcLong( 0x0 ),	/* 0 */
/* 530 */	NdrFcShort( 0x1 ),	/* 1 */
/* 532 */	NdrFcShort( 0x4 ),	/* x86 Stack size/offset = 4 */
/* 534 */	0x32,		/* FC_BIND_PRIMITIVE */
			0x0,		/* 0 */
/* 536 */	NdrFcShort( 0x0 ),	/* x86 Stack size/offset = 0 */
/* 538 */	NdrFcShort( 0x0 ),	/* 0 */
/* 540 */	NdrFcShort( 0x0 ),	/* 0 */
/* 542 */	0x40,		/* Oi2 Flags:  has ext, */
			0x0,		/* 0 */
/* 544 */	0x8,		/* 8 */
			0x1,		/* Ext Flags:  new corr desc, */
/* 546 */	NdrFcShort( 0x0 ),	/* 0 */
/* 548 */	NdrFcShort( 0x0 ),	/* 0 */
/* 550 */	NdrFcShort( 0x0 ),	/* 0 */

	/* Procedure Opnum2NotUsedOnWire */


	/* Parameter rpc */

/* 552 */	0x0,		/* 0 */
			0x48,		/* Old Flags:  */
/* 554 */	NdrFcLong( 0x0 ),	/* 0 */
/* 558 */	NdrFcShort( 0x2 ),	/* 2 */
/* 560 */	NdrFcShort( 0x4 ),	/* x86 Stack size/offset = 4 */
/* 562 */	0x32,		/* FC_BIND_PRIMITIVE */
			0x0,		/* 0 */
/* 564 */	NdrFcShort( 0x0 ),	/* x86 Stack size/offset = 0 */
/* 566 */	NdrFcShort( 0x0 ),	/* 0 */
/* 568 */	NdrFcShort( 0x0 ),	/* 0 */
/* 570 */	0x40,		/* Oi2 Flags:  has ext, */
			0x0,		/* 0 */
/* 572 */	0x8,		/* 8 */
			0x1,		/* Ext Flags:  new corr desc, */
/* 574 */	NdrFcShort( 0x0 ),	/* 0 */
/* 576 */	NdrFcShort( 0x0 ),	/* 0 */
/* 578 */	NdrFcShort( 0x0 ),	/* 0 */

	/* Procedure RemoteGetClassObject */


	/* Parameter rpc */

/* 580 */	0x0,		/* 0 */
			0x48,		/* Old Flags:  */
/* 582 */	NdrFcLong( 0x0 ),	/* 0 */
/* 586 */	NdrFcShort( 0x3 ),	/* 3 */
/* 588 */	NdrFcShort( 0x18 ),	/* x86 Stack size/offset = 24 */
/* 590 */	0x32,		/* FC_BIND_PRIMITIVE */
			0x0,		/* 0 */
/* 592 */	NdrFcShort( 0x0 ),	/* x86 Stack size/offset = 0 */
/* 594 */	NdrFcShort( 0x0 ),	/* 0 */
/* 596 */	NdrFcShort( 0x8 ),	/* 8 */
/* 598 */	0x47,		/* Oi2 Flags:  srv must size, clt must size, has return, has ext, */
			0x5,		/* 5 */
/* 600 */	0x8,		/* 8 */
			0x7,		/* Ext Flags:  new corr desc, clt corr check, srv corr check, */
/* 602 */	NdrFcShort( 0x1 ),	/* 1 */
/* 604 */	NdrFcShort( 0x1 ),	/* 1 */
/* 606 */	NdrFcShort( 0x0 ),	/* 0 */

	/* Parameter rpc */

/* 608 */	NdrFcShort( 0x10b ),	/* Flags:  must size, must free, in, simple ref, */
/* 610 */	NdrFcShort( 0x4 ),	/* x86 Stack size/offset = 4 */
/* 612 */	NdrFcShort( 0xcc ),	/* Type Offset=204 */

	/* Parameter orpcthis */

/* 614 */	NdrFcShort( 0x2113 ),	/* Flags:  must size, must free, out, simple ref, srv alloc size=8 */
/* 616 */	NdrFcShort( 0x8 ),	/* x86 Stack size/offset = 8 */
/* 618 */	NdrFcShort( 0xee ),	/* Type Offset=238 */

	/* Parameter orpcthat */

/* 620 */	NdrFcShort( 0xb ),	/* Flags:  must size, must free, in, */
/* 622 */	NdrFcShort( 0xc ),	/* x86 Stack size/offset = 12 */
/* 624 */	NdrFcShort( 0x10a ),	/* Type Offset=266 */

	/* Parameter pActProperties */

/* 626 */	NdrFcShort( 0x2013 ),	/* Flags:  must size, must free, out, srv alloc size=8 */
/* 628 */	NdrFcShort( 0x10 ),	/* x86 Stack size/offset = 16 */
/* 630 */	NdrFcShort( 0x18e ),	/* Type Offset=398 */

	/* Parameter ppActProperties */

/* 632 */	NdrFcShort( 0x70 ),	/* Flags:  out, return, base type, */
/* 634 */	NdrFcShort( 0x14 ),	/* x86 Stack size/offset = 20 */
/* 636 */	0x8,		/* FC_LONG */
			0x0,		/* 0 */

	/* Procedure RemoteCreateInstance */


	/* Return value */

/* 638 */	0x0,		/* 0 */
			0x48,		/* Old Flags:  */
/* 640 */	NdrFcLong( 0x0 ),	/* 0 */
/* 644 */	NdrFcShort( 0x4 ),	/* 4 */
/* 646 */	NdrFcShort( 0x1c ),	/* x86 Stack size/offset = 28 */
/* 648 */	0x32,		/* FC_BIND_PRIMITIVE */
			0x0,		/* 0 */
/* 650 */	NdrFcShort( 0x0 ),	/* x86 Stack size/offset = 0 */
/* 652 */	NdrFcShort( 0x0 ),	/* 0 */
/* 654 */	NdrFcShort( 0x8 ),	/* 8 */
/* 656 */	0x47,		/* Oi2 Flags:  srv must size, clt must size, has return, has ext, */
			0x6,		/* 6 */
/* 658 */	0x8,		/* 8 */
			0x7,		/* Ext Flags:  new corr desc, clt corr check, srv corr check, */
/* 660 */	NdrFcShort( 0x1 ),	/* 1 */
/* 662 */	NdrFcShort( 0x1 ),	/* 1 */
/* 664 */	NdrFcShort( 0x0 ),	/* 0 */

	/* Parameter rpc */

/* 666 */	NdrFcShort( 0x10b ),	/* Flags:  must size, must free, in, simple ref, */
/* 668 */	NdrFcShort( 0x4 ),	/* x86 Stack size/offset = 4 */
/* 670 */	NdrFcShort( 0xcc ),	/* Type Offset=204 */

	/* Parameter orpcthis */

/* 672 */	NdrFcShort( 0x2113 ),	/* Flags:  must size, must free, out, simple ref, srv alloc size=8 */
/* 674 */	NdrFcShort( 0x8 ),	/* x86 Stack size/offset = 8 */
/* 676 */	NdrFcShort( 0xee ),	/* Type Offset=238 */

	/* Parameter orpcthat */

/* 678 */	NdrFcShort( 0xb ),	/* Flags:  must size, must free, in, */
/* 680 */	NdrFcShort( 0xc ),	/* x86 Stack size/offset = 12 */
/* 682 */	NdrFcShort( 0x10a ),	/* Type Offset=266 */

	/* Parameter pUnkOuter */

/* 684 */	NdrFcShort( 0xb ),	/* Flags:  must size, must free, in, */
/* 686 */	NdrFcShort( 0x10 ),	/* x86 Stack size/offset = 16 */
/* 688 */	NdrFcShort( 0x10a ),	/* Type Offset=266 */

	/* Parameter pActProperties */

/* 690 */	NdrFcShort( 0x2013 ),	/* Flags:  must size, must free, out, srv alloc size=8 */
/* 692 */	NdrFcShort( 0x14 ),	/* x86 Stack size/offset = 20 */
/* 694 */	NdrFcShort( 0x18e ),	/* Type Offset=398 */

	/* Parameter ppActProperties */

/* 696 */	NdrFcShort( 0x70 ),	/* Flags:  out, return, base type, */
/* 698 */	NdrFcShort( 0x18 ),	/* x86 Stack size/offset = 24 */
/* 700 */	0x8,		/* FC_LONG */
			0x0,		/* 0 */

	/* Procedure RemQueryInterface */


	/* Return value */

/* 702 */	0x33,		/* FC_AUTO_HANDLE */
			0x6c,		/* Old Flags:  object, Oi2 */
/* 704 */	NdrFcLong( 0x0 ),	/* 0 */
/* 708 */	NdrFcShort( 0x3 ),	/* 3 */
/* 710 */	NdrFcShort( 0x1c ),	/* x86 Stack size/offset = 28 */
/* 712 */	NdrFcShort( 0x52 ),	/* 82 */
/* 714 */	NdrFcShort( 0x8 ),	/* 8 */
/* 716 */	0x47,		/* Oi2 Flags:  srv must size, clt must size, has return, has ext, */
			0x6,		/* 6 */
/* 718 */	0x8,		/* 8 */
			0x7,		/* Ext Flags:  new corr desc, clt corr check, srv corr check, */
/* 720 */	NdrFcShort( 0x1 ),	/* 1 */
/* 722 */	NdrFcShort( 0x1 ),	/* 1 */
/* 724 */	NdrFcShort( 0x0 ),	/* 0 */

	/* Parameter ripid */

/* 726 */	NdrFcShort( 0x10a ),	/* Flags:  must free, in, simple ref, */
/* 728 */	NdrFcShort( 0x4 ),	/* x86 Stack size/offset = 4 */
/* 730 */	NdrFcShort( 0x3a ),	/* Type Offset=58 */

	/* Parameter cRefs */

/* 732 */	NdrFcShort( 0x48 ),	/* Flags:  in, base type, */
/* 734 */	NdrFcShort( 0x8 ),	/* x86 Stack size/offset = 8 */
/* 736 */	0x8,		/* FC_LONG */
			0x0,		/* 0 */

	/* Parameter cIids */

/* 738 */	NdrFcShort( 0x48 ),	/* Flags:  in, base type, */
/* 740 */	NdrFcShort( 0xc ),	/* x86 Stack size/offset = 12 */
/* 742 */	0x6,		/* FC_SHORT */
			0x0,		/* 0 */

	/* Parameter iids */

/* 744 */	NdrFcShort( 0x10b ),	/* Flags:  must size, must free, in, simple ref, */
/* 746 */	NdrFcShort( 0x10 ),	/* x86 Stack size/offset = 16 */
/* 748 */	NdrFcShort( 0x196 ),	/* Type Offset=406 */

	/* Parameter ppQIResults */

/* 750 */	NdrFcShort( 0x2013 ),	/* Flags:  must size, must free, out, srv alloc size=8 */
/* 752 */	NdrFcShort( 0x14 ),	/* x86 Stack size/offset = 20 */
/* 754 */	NdrFcShort( 0x1a6 ),	/* Type Offset=422 */

	/* Return value */

/* 756 */	NdrFcShort( 0x70 ),	/* Flags:  out, return, base type, */
/* 758 */	NdrFcShort( 0x18 ),	/* x86 Stack size/offset = 24 */
/* 760 */	0x8,		/* FC_LONG */
			0x0,		/* 0 */

	/* Procedure RemAddRef */

/* 762 */	0x33,		/* FC_AUTO_HANDLE */
			0x6c,		/* Old Flags:  object, Oi2 */
/* 764 */	NdrFcLong( 0x0 ),	/* 0 */
/* 768 */	NdrFcShort( 0x4 ),	/* 4 */
/* 770 */	NdrFcShort( 0x14 ),	/* x86 Stack size/offset = 20 */
/* 772 */	NdrFcShort( 0x6 ),	/* 6 */
/* 774 */	NdrFcShort( 0x8 ),	/* 8 */
/* 776 */	0x47,		/* Oi2 Flags:  srv must size, clt must size, has return, has ext, */
			0x4,		/* 4 */
/* 778 */	0x8,		/* 8 */
			0x7,		/* Ext Flags:  new corr desc, clt corr check, srv corr check, */
/* 780 */	NdrFcShort( 0x1 ),	/* 1 */
/* 782 */	NdrFcShort( 0x1 ),	/* 1 */
/* 784 */	NdrFcShort( 0x0 ),	/* 0 */

	/* Parameter cInterfaceRefs */

/* 786 */	NdrFcShort( 0x48 ),	/* Flags:  in, base type, */
/* 788 */	NdrFcShort( 0x4 ),	/* x86 Stack size/offset = 4 */
/* 790 */	0x6,		/* FC_SHORT */
			0x0,		/* 0 */

	/* Parameter InterfaceRefs */

/* 792 */	NdrFcShort( 0xb ),	/* Flags:  must size, must free, in, */
/* 794 */	NdrFcShort( 0x8 ),	/* x86 Stack size/offset = 8 */
/* 796 */	NdrFcShort( 0x1e4 ),	/* Type Offset=484 */

	/* Parameter pResults */

/* 798 */	NdrFcShort( 0x113 ),	/* Flags:  must size, must free, out, simple ref, */
/* 800 */	NdrFcShort( 0xc ),	/* x86 Stack size/offset = 12 */
/* 802 */	NdrFcShort( 0x1f8 ),	/* Type Offset=504 */

	/* Return value */

/* 804 */	NdrFcShort( 0x70 ),	/* Flags:  out, return, base type, */
/* 806 */	NdrFcShort( 0x10 ),	/* x86 Stack size/offset = 16 */
/* 808 */	0x8,		/* FC_LONG */
			0x0,		/* 0 */

	/* Procedure RemRelease */

/* 810 */	0x33,		/* FC_AUTO_HANDLE */
			0x6c,		/* Old Flags:  object, Oi2 */
/* 812 */	NdrFcLong( 0x0 ),	/* 0 */
/* 816 */	NdrFcShort( 0x5 ),	/* 5 */
/* 818 */	NdrFcShort( 0x10 ),	/* x86 Stack size/offset = 16 */
/* 820 */	NdrFcShort( 0x6 ),	/* 6 */
/* 822 */	NdrFcShort( 0x8 ),	/* 8 */
/* 824 */	0x46,		/* Oi2 Flags:  clt must size, has return, has ext, */
			0x3,		/* 3 */
/* 826 */	0x8,		/* 8 */
			0x5,		/* Ext Flags:  new corr desc, srv corr check, */
/* 828 */	NdrFcShort( 0x0 ),	/* 0 */
/* 830 */	NdrFcShort( 0x1 ),	/* 1 */
/* 832 */	NdrFcShort( 0x0 ),	/* 0 */

	/* Parameter cInterfaceRefs */

/* 834 */	NdrFcShort( 0x48 ),	/* Flags:  in, base type, */
/* 836 */	NdrFcShort( 0x4 ),	/* x86 Stack size/offset = 4 */
/* 838 */	0x6,		/* FC_SHORT */
			0x0,		/* 0 */

	/* Parameter InterfaceRefs */

/* 840 */	NdrFcShort( 0xb ),	/* Flags:  must size, must free, in, */
/* 842 */	NdrFcShort( 0x8 ),	/* x86 Stack size/offset = 8 */
/* 844 */	NdrFcShort( 0x1e4 ),	/* Type Offset=484 */

	/* Return value */

/* 846 */	NdrFcShort( 0x70 ),	/* Flags:  out, return, base type, */
/* 848 */	NdrFcShort( 0xc ),	/* x86 Stack size/offset = 12 */
/* 850 */	0x8,		/* FC_LONG */
			0x0,		/* 0 */

	/* Procedure RemQueryInterface2 */

/* 852 */	0x33,		/* FC_AUTO_HANDLE */
			0x6c,		/* Old Flags:  object, Oi2 */
/* 854 */	NdrFcLong( 0x0 ),	/* 0 */
/* 858 */	NdrFcShort( 0x6 ),	/* 6 */
/* 860 */	NdrFcShort( 0x1c ),	/* x86 Stack size/offset = 28 */
/* 862 */	NdrFcShort( 0x4a ),	/* 74 */
/* 864 */	NdrFcShort( 0x8 ),	/* 8 */
/* 866 */	0x47,		/* Oi2 Flags:  srv must size, clt must size, has return, has ext, */
			0x6,		/* 6 */
/* 868 */	0x8,		/* 8 */
			0x7,		/* Ext Flags:  new corr desc, clt corr check, srv corr check, */
/* 870 */	NdrFcShort( 0x1 ),	/* 1 */
/* 872 */	NdrFcShort( 0x1 ),	/* 1 */
/* 874 */	NdrFcShort( 0x0 ),	/* 0 */

	/* Parameter ripid */

/* 876 */	NdrFcShort( 0x10a ),	/* Flags:  must free, in, simple ref, */
/* 878 */	NdrFcShort( 0x4 ),	/* x86 Stack size/offset = 4 */
/* 880 */	NdrFcShort( 0x3a ),	/* Type Offset=58 */

	/* Parameter cIids */

/* 882 */	NdrFcShort( 0x48 ),	/* Flags:  in, base type, */
/* 884 */	NdrFcShort( 0x8 ),	/* x86 Stack size/offset = 8 */
/* 886 */	0x6,		/* FC_SHORT */
			0x0,		/* 0 */

	/* Parameter iids */

/* 888 */	NdrFcShort( 0x10b ),	/* Flags:  must size, must free, in, simple ref, */
/* 890 */	NdrFcShort( 0xc ),	/* x86 Stack size/offset = 12 */
/* 892 */	NdrFcShort( 0x208 ),	/* Type Offset=520 */

	/* Parameter phr */

/* 894 */	NdrFcShort( 0x113 ),	/* Flags:  must size, must free, out, simple ref, */
/* 896 */	NdrFcShort( 0x10 ),	/* x86 Stack size/offset = 16 */
/* 898 */	NdrFcShort( 0x21c ),	/* Type Offset=540 */

	/* Parameter ppMIF */

/* 900 */	NdrFcShort( 0x113 ),	/* Flags:  must size, must free, out, simple ref, */
/* 902 */	NdrFcShort( 0x14 ),	/* x86 Stack size/offset = 20 */
/* 904 */	NdrFcShort( 0x22c ),	/* Type Offset=556 */

	/* Return value */

/* 906 */	NdrFcShort( 0x70 ),	/* Flags:  out, return, base type, */
/* 908 */	NdrFcShort( 0x18 ),	/* x86 Stack size/offset = 24 */
/* 910 */	0x8,		/* FC_LONG */
			0x0,		/* 0 */

			0x0
        }
    };

static const objectexporter_MIDL_TYPE_FORMAT_STRING objectexporter__MIDL_TypeFormatString =
    {
        0,
        {
			NdrFcShort( 0x0 ),	/* 0 */
/*  2 */	
			0x11, 0x8,	/* FC_RP [simple_pointer] */
/*  4 */	0xb,		/* FC_HYPER */
			0x5c,		/* FC_PAD */
/*  6 */	
			0x1b,		/* FC_CARRAY */
			0x1,		/* 1 */
/*  8 */	NdrFcShort( 0x2 ),	/* 2 */
/* 10 */	0x27,		/* Corr desc:  parameter, FC_USHORT */
			0x0,		/*  */
/* 12 */	NdrFcShort( 0x8 ),	/* x86 Stack size/offset = 8 */
/* 14 */	NdrFcShort( 0x1 ),	/* Corr flags:  early, */
/* 16 */	0x6,		/* FC_SHORT */
			0x5b,		/* FC_END */
/* 18 */	
			0x11, 0x14,	/* FC_RP [alloced_on_stack] [pointer_deref] */
/* 20 */	NdrFcShort( 0x2 ),	/* Offset= 2 (22) */
/* 22 */	
			0x12, 0x0,	/* FC_UP */
/* 24 */	NdrFcShort( 0xe ),	/* Offset= 14 (38) */
/* 26 */	
			0x1b,		/* FC_CARRAY */
			0x1,		/* 1 */
/* 28 */	NdrFcShort( 0x2 ),	/* 2 */
/* 30 */	0x7,		/* Corr desc: FC_USHORT */
			0x0,		/*  */
/* 32 */	NdrFcShort( 0xfffc ),	/* -4 */
/* 34 */	NdrFcShort( 0x1 ),	/* Corr flags:  early, */
/* 36 */	0x6,		/* FC_SHORT */
			0x5b,		/* FC_END */
/* 38 */	
			0x17,		/* FC_CSTRUCT */
			0x1,		/* 1 */
/* 40 */	NdrFcShort( 0x4 ),	/* 4 */
/* 42 */	NdrFcShort( 0xfff0 ),	/* Offset= -16 (26) */
/* 44 */	0x6,		/* FC_SHORT */
			0x6,		/* FC_SHORT */
/* 46 */	0x5c,		/* FC_PAD */
			0x5b,		/* FC_END */
/* 48 */	
			0x11, 0x4,	/* FC_RP [alloced_on_stack] */
/* 50 */	NdrFcShort( 0x8 ),	/* Offset= 8 (58) */
/* 52 */	
			0x1d,		/* FC_SMFARRAY */
			0x0,		/* 0 */
/* 54 */	NdrFcShort( 0x8 ),	/* 8 */
/* 56 */	0x1,		/* FC_BYTE */
			0x5b,		/* FC_END */
/* 58 */	
			0x15,		/* FC_STRUCT */
			0x3,		/* 3 */
/* 60 */	NdrFcShort( 0x10 ),	/* 16 */
/* 62 */	0x8,		/* FC_LONG */
			0x6,		/* FC_SHORT */
/* 64 */	0x6,		/* FC_SHORT */
			0x4c,		/* FC_EMBEDDED_COMPLEX */
/* 66 */	0x0,		/* 0 */
			NdrFcShort( 0xfff1 ),	/* Offset= -15 (52) */
			0x5b,		/* FC_END */
/* 70 */	
			0x11, 0xc,	/* FC_RP [alloced_on_stack] [simple_pointer] */
/* 72 */	0x8,		/* FC_LONG */
			0x5c,		/* FC_PAD */
/* 74 */	
			0x12,		/* FC_UP */
			0x0,		/* 0 */
/* 76 */	NdrFcShort( 0x2 ),	/* Offset= 2 (78) */
/* 78 */	
			0x1b,		/* FC_CARRAY */
			0x7,		/* 7 */
/* 80 */	NdrFcShort( 0x8 ),	/* 8 */
/* 82 */	0x27,		/* Corr desc:  parameter, FC_USHORT */
			0x0,		/*  */
/* 84 */	NdrFcShort( 0xc ),	/* x86 Stack size/offset = 12 */
/* 86 */	NdrFcShort( 0x1 ),	/* Corr flags:  early, */
/* 88 */	0xb,		/* FC_HYPER */
			0x5b,		/* FC_END */
/* 90 */	
			0x12,		/* FC_UP */
			0x0,		/* 0 */
/* 92 */	NdrFcShort( 0x2 ),	/* Offset= 2 (94) */
/* 94 */	
			0x1b,		/* FC_CARRAY */
			0x7,		/* 7 */
/* 96 */	NdrFcShort( 0x8 ),	/* 8 */
/* 98 */	0x27,		/* Corr desc:  parameter, FC_USHORT */
			0x0,		/*  */
/* 100 */	NdrFcShort( 0x10 ),	/* x86 Stack size/offset = 16 */
/* 102 */	NdrFcShort( 0x1 ),	/* Corr flags:  early, */
/* 104 */	0xb,		/* FC_HYPER */
			0x5b,		/* FC_END */
/* 106 */	
			0x11, 0xc,	/* FC_RP [alloced_on_stack] [simple_pointer] */
/* 108 */	0x6,		/* FC_SHORT */
			0x5c,		/* FC_PAD */
/* 110 */	
			0x11, 0x4,	/* FC_RP [alloced_on_stack] */
/* 112 */	NdrFcShort( 0x2 ),	/* Offset= 2 (114) */
/* 114 */	
			0x15,		/* FC_STRUCT */
			0x1,		/* 1 */
/* 116 */	NdrFcShort( 0x4 ),	/* 4 */
/* 118 */	0x6,		/* FC_SHORT */
			0x6,		/* FC_SHORT */
/* 120 */	0x5c,		/* FC_PAD */
			0x5b,		/* FC_END */
/* 122 */	
			0x11, 0x0,	/* FC_RP */
/* 124 */	NdrFcShort( 0x50 ),	/* Offset= 80 (204) */
/* 126 */	
			0x1b,		/* FC_CARRAY */
			0x0,		/* 0 */
/* 128 */	NdrFcShort( 0x1 ),	/* 1 */
/* 130 */	0x0,		/* Corr desc:  field,  */
			0x59,		/* FC_CALLBACK */
/* 132 */	NdrFcShort( 0x0 ),	/* 0 */
/* 134 */	NdrFcShort( 0x0 ),	/* Corr flags:  */
/* 136 */	0x1,		/* FC_BYTE */
			0x5b,		/* FC_END */
/* 138 */	
			0x17,		/* FC_CSTRUCT */
			0x3,		/* 3 */
/* 140 */	NdrFcShort( 0x14 ),	/* 20 */
/* 142 */	NdrFcShort( 0xfff0 ),	/* Offset= -16 (126) */
/* 144 */	0x4c,		/* FC_EMBEDDED_COMPLEX */
			0x0,		/* 0 */
/* 146 */	NdrFcShort( 0xffa8 ),	/* Offset= -88 (58) */
/* 148 */	0x8,		/* FC_LONG */
			0x5b,		/* FC_END */
/* 150 */	
			0x1b,		/* FC_CARRAY */
			0x3,		/* 3 */
/* 152 */	NdrFcShort( 0x4 ),	/* 4 */
/* 154 */	0x10,		/* Corr desc:  field pointer,  */
			0x59,		/* FC_CALLBACK */
/* 156 */	NdrFcShort( 0x1 ),	/* 1 */
/* 158 */	NdrFcShort( 0x0 ),	/* Corr flags:  */
/* 160 */	
			0x4b,		/* FC_PP */
			0x5c,		/* FC_PAD */
/* 162 */	
			0x48,		/* FC_VARIABLE_REPEAT */
			0x49,		/* FC_FIXED_OFFSET */
/* 164 */	NdrFcShort( 0x4 ),	/* 4 */
/* 166 */	NdrFcShort( 0x0 ),	/* 0 */
/* 168 */	NdrFcShort( 0x1 ),	/* 1 */
/* 170 */	NdrFcShort( 0x0 ),	/* 0 */
/* 172 */	NdrFcShort( 0x0 ),	/* 0 */
/* 174 */	0x12, 0x0,	/* FC_UP */
/* 176 */	NdrFcShort( 0xffda ),	/* Offset= -38 (138) */
/* 178 */	
			0x5b,		/* FC_END */

			0x8,		/* FC_LONG */
/* 180 */	0x5c,		/* FC_PAD */
			0x5b,		/* FC_END */
/* 182 */	
			0x16,		/* FC_PSTRUCT */
			0x3,		/* 3 */
/* 184 */	NdrFcShort( 0xc ),	/* 12 */
/* 186 */	
			0x4b,		/* FC_PP */
			0x5c,		/* FC_PAD */
/* 188 */	
			0x46,		/* FC_NO_REPEAT */
			0x5c,		/* FC_PAD */
/* 190 */	NdrFcShort( 0x8 ),	/* 8 */
/* 192 */	NdrFcShort( 0x8 ),	/* 8 */
/* 194 */	0x12, 0x0,	/* FC_UP */
/* 196 */	NdrFcShort( 0xffd2 ),	/* Offset= -46 (150) */
/* 198 */	
			0x5b,		/* FC_END */

			0x8,		/* FC_LONG */
/* 200 */	0x8,		/* FC_LONG */
			0x8,		/* FC_LONG */
/* 202 */	0x5c,		/* FC_PAD */
			0x5b,		/* FC_END */
/* 204 */	
			0x16,		/* FC_PSTRUCT */
			0x3,		/* 3 */
/* 206 */	NdrFcShort( 0x20 ),	/* 32 */
/* 208 */	
			0x4b,		/* FC_PP */
			0x5c,		/* FC_PAD */
/* 210 */	
			0x46,		/* FC_NO_REPEAT */
			0x5c,		/* FC_PAD */
/* 212 */	NdrFcShort( 0x1c ),	/* 28 */
/* 214 */	NdrFcShort( 0x1c ),	/* 28 */
/* 216 */	0x12, 0x0,	/* FC_UP */
/* 218 */	NdrFcShort( 0xffdc ),	/* Offset= -36 (182) */
/* 220 */	
			0x5b,		/* FC_END */

			0x4c,		/* FC_EMBEDDED_COMPLEX */
/* 222 */	0x0,		/* 0 */
			NdrFcShort( 0xff93 ),	/* Offset= -109 (114) */
			0x8,		/* FC_LONG */
/* 226 */	0x8,		/* FC_LONG */
			0x4c,		/* FC_EMBEDDED_COMPLEX */
/* 228 */	0x0,		/* 0 */
			NdrFcShort( 0xff55 ),	/* Offset= -171 (58) */
			0x8,		/* FC_LONG */
/* 232 */	0x5c,		/* FC_PAD */
			0x5b,		/* FC_END */
/* 234 */	
			0x11, 0x4,	/* FC_RP [alloced_on_stack] */
/* 236 */	NdrFcShort( 0x2 ),	/* Offset= 2 (238) */
/* 238 */	
			0x16,		/* FC_PSTRUCT */
			0x3,		/* 3 */
/* 240 */	NdrFcShort( 0x8 ),	/* 8 */
/* 242 */	
			0x4b,		/* FC_PP */
			0x5c,		/* FC_PAD */
/* 244 */	
			0x46,		/* FC_NO_REPEAT */
			0x5c,		/* FC_PAD */
/* 246 */	NdrFcShort( 0x4 ),	/* 4 */
/* 248 */	NdrFcShort( 0x4 ),	/* 4 */
/* 250 */	0x12, 0x0,	/* FC_UP */
/* 252 */	NdrFcShort( 0xffba ),	/* Offset= -70 (182) */
/* 254 */	
			0x5b,		/* FC_END */

			0x8,		/* FC_LONG */
/* 256 */	0x8,		/* FC_LONG */
			0x5b,		/* FC_END */
/* 258 */	
			0x11, 0x0,	/* FC_RP */
/* 260 */	NdrFcShort( 0xff36 ),	/* Offset= -202 (58) */
/* 262 */	
			0x12, 0x8,	/* FC_UP [simple_pointer] */
/* 264 */	
			0x25,		/* FC_C_WSTRING */
			0x5c,		/* FC_PAD */
/* 266 */	
			0x12, 0x0,	/* FC_UP */
/* 268 */	NdrFcShort( 0xe ),	/* Offset= 14 (282) */
/* 270 */	
			0x1b,		/* FC_CARRAY */
			0x0,		/* 0 */
/* 272 */	NdrFcShort( 0x1 ),	/* 1 */
/* 274 */	0x9,		/* Corr desc: FC_ULONG */
			0x0,		/*  */
/* 276 */	NdrFcShort( 0xfffc ),	/* -4 */
/* 278 */	NdrFcShort( 0x1 ),	/* Corr flags:  early, */
/* 280 */	0x1,		/* FC_BYTE */
			0x5b,		/* FC_END */
/* 282 */	
			0x17,		/* FC_CSTRUCT */
			0x3,		/* 3 */
/* 284 */	NdrFcShort( 0x4 ),	/* 4 */
/* 286 */	NdrFcShort( 0xfff0 ),	/* Offset= -16 (270) */
/* 288 */	0x8,		/* FC_LONG */
			0x5b,		/* FC_END */
/* 290 */	0xb7,		/* FC_RANGE */
			0x8,		/* 8 */
/* 292 */	NdrFcLong( 0x1 ),	/* 1 */
/* 296 */	NdrFcLong( 0x8000 ),	/* 32768 */
/* 300 */	
			0x12, 0x0,	/* FC_UP */
/* 302 */	NdrFcShort( 0x2 ),	/* Offset= 2 (304) */
/* 304 */	
			0x1b,		/* FC_CARRAY */
			0x3,		/* 3 */
/* 306 */	NdrFcShort( 0x10 ),	/* 16 */
/* 308 */	0x29,		/* Corr desc:  parameter, FC_ULONG */
			0x0,		/*  */
/* 310 */	NdrFcShort( 0x20 ),	/* x86 Stack size/offset = 32 */
/* 312 */	NdrFcShort( 0x1 ),	/* Corr flags:  early, */
/* 314 */	0x4c,		/* FC_EMBEDDED_COMPLEX */
			0x0,		/* 0 */
/* 316 */	NdrFcShort( 0xfefe ),	/* Offset= -258 (58) */
/* 318 */	0x5c,		/* FC_PAD */
			0x5b,		/* FC_END */
/* 320 */	0xb7,		/* FC_RANGE */
			0x6,		/* 6 */
/* 322 */	NdrFcLong( 0x0 ),	/* 0 */
/* 326 */	NdrFcLong( 0x8000 ),	/* 32768 */
/* 330 */	
			0x1b,		/* FC_CARRAY */
			0x1,		/* 1 */
/* 332 */	NdrFcShort( 0x2 ),	/* 2 */
/* 334 */	0x27,		/* Corr desc:  parameter, FC_USHORT */
			0x0,		/*  */
/* 336 */	NdrFcShort( 0x28 ),	/* x86 Stack size/offset = 40 */
/* 338 */	NdrFcShort( 0x1 ),	/* Corr flags:  early, */
/* 340 */	0x6,		/* FC_SHORT */
			0x5b,		/* FC_END */
/* 342 */	
			0x11, 0xc,	/* FC_RP [alloced_on_stack] [simple_pointer] */
/* 344 */	0xb,		/* FC_HYPER */
			0x5c,		/* FC_PAD */
/* 346 */	
			0x11, 0x0,	/* FC_RP */
/* 348 */	NdrFcShort( 0x2 ),	/* Offset= 2 (350) */
/* 350 */	
			0x1b,		/* FC_CARRAY */
			0x3,		/* 3 */
/* 352 */	NdrFcShort( 0x4 ),	/* 4 */
/* 354 */	0x29,		/* Corr desc:  parameter, FC_ULONG */
			0x0,		/*  */
/* 356 */	NdrFcShort( 0x20 ),	/* x86 Stack size/offset = 32 */
/* 358 */	NdrFcShort( 0x1 ),	/* Corr flags:  early, */
/* 360 */	
			0x4b,		/* FC_PP */
			0x5c,		/* FC_PAD */
/* 362 */	
			0x48,		/* FC_VARIABLE_REPEAT */
			0x49,		/* FC_FIXED_OFFSET */
/* 364 */	NdrFcShort( 0x4 ),	/* 4 */
/* 366 */	NdrFcShort( 0x0 ),	/* 0 */
/* 368 */	NdrFcShort( 0x1 ),	/* 1 */
/* 370 */	NdrFcShort( 0x0 ),	/* 0 */
/* 372 */	NdrFcShort( 0x0 ),	/* 0 */
/* 374 */	0x12, 0x0,	/* FC_UP */
/* 376 */	NdrFcShort( 0xffa2 ),	/* Offset= -94 (282) */
/* 378 */	
			0x5b,		/* FC_END */

			0x8,		/* FC_LONG */
/* 380 */	0x5c,		/* FC_PAD */
			0x5b,		/* FC_END */
/* 382 */	
			0x11, 0x0,	/* FC_RP */
/* 384 */	NdrFcShort( 0x2 ),	/* Offset= 2 (386) */
/* 386 */	
			0x1b,		/* FC_CARRAY */
			0x3,		/* 3 */
/* 388 */	NdrFcShort( 0x4 ),	/* 4 */
/* 390 */	0x29,		/* Corr desc:  parameter, FC_ULONG */
			0x0,		/*  */
/* 392 */	NdrFcShort( 0x20 ),	/* x86 Stack size/offset = 32 */
/* 394 */	NdrFcShort( 0x1 ),	/* Corr flags:  early, */
/* 396 */	0x8,		/* FC_LONG */
			0x5b,		/* FC_END */
/* 398 */	
			0x11, 0x14,	/* FC_RP [alloced_on_stack] [pointer_deref] */
/* 400 */	NdrFcShort( 0xff7a ),	/* Offset= -134 (266) */
/* 402 */	
			0x11, 0x0,	/* FC_RP */
/* 404 */	NdrFcShort( 0x2 ),	/* Offset= 2 (406) */
/* 406 */	
			0x1b,		/* FC_CARRAY */
			0x3,		/* 3 */
/* 408 */	NdrFcShort( 0x10 ),	/* 16 */
/* 410 */	0x27,		/* Corr desc:  parameter, FC_USHORT */
			0x0,		/*  */
/* 412 */	NdrFcShort( 0xc ),	/* x86 Stack size/offset = 12 */
/* 414 */	NdrFcShort( 0x1 ),	/* Corr flags:  early, */
/* 416 */	0x4c,		/* FC_EMBEDDED_COMPLEX */
			0x0,		/* 0 */
/* 418 */	NdrFcShort( 0xfe98 ),	/* Offset= -360 (58) */
/* 420 */	0x5c,		/* FC_PAD */
			0x5b,		/* FC_END */
/* 422 */	
			0x11, 0x14,	/* FC_RP [alloced_on_stack] [pointer_deref] */
/* 424 */	NdrFcShort( 0x2 ),	/* Offset= 2 (426) */
/* 426 */	
			0x13, 0x0,	/* FC_OP */
/* 428 */	NdrFcShort( 0x1c ),	/* Offset= 28 (456) */
/* 430 */	
			0x15,		/* FC_STRUCT */
			0x7,		/* 7 */
/* 432 */	NdrFcShort( 0x28 ),	/* 40 */
/* 434 */	0x8,		/* FC_LONG */
			0x8,		/* FC_LONG */
/* 436 */	0xb,		/* FC_HYPER */
			0xb,		/* FC_HYPER */
/* 438 */	0x4c,		/* FC_EMBEDDED_COMPLEX */
			0x0,		/* 0 */
/* 440 */	NdrFcShort( 0xfe82 ),	/* Offset= -382 (58) */
/* 442 */	0x5c,		/* FC_PAD */
			0x5b,		/* FC_END */
/* 444 */	
			0x15,		/* FC_STRUCT */
			0x7,		/* 7 */
/* 446 */	NdrFcShort( 0x30 ),	/* 48 */
/* 448 */	0x8,		/* FC_LONG */
			0x40,		/* FC_STRUCTPAD4 */
/* 450 */	0x4c,		/* FC_EMBEDDED_COMPLEX */
			0x0,		/* 0 */
/* 452 */	NdrFcShort( 0xffea ),	/* Offset= -22 (430) */
/* 454 */	0x5c,		/* FC_PAD */
			0x5b,		/* FC_END */
/* 456 */	
			0x1b,		/* FC_CARRAY */
			0x7,		/* 7 */
/* 458 */	NdrFcShort( 0x30 ),	/* 48 */
/* 460 */	0x27,		/* Corr desc:  parameter, FC_USHORT */
			0x0,		/*  */
/* 462 */	NdrFcShort( 0xc ),	/* x86 Stack size/offset = 12 */
/* 464 */	NdrFcShort( 0x1 ),	/* Corr flags:  early, */
/* 466 */	0x4c,		/* FC_EMBEDDED_COMPLEX */
			0x0,		/* 0 */
/* 468 */	NdrFcShort( 0xffe8 ),	/* Offset= -24 (444) */
/* 470 */	0x5c,		/* FC_PAD */
			0x5b,		/* FC_END */
/* 472 */	
			0x15,		/* FC_STRUCT */
			0x3,		/* 3 */
/* 474 */	NdrFcShort( 0x18 ),	/* 24 */
/* 476 */	0x4c,		/* FC_EMBEDDED_COMPLEX */
			0x0,		/* 0 */
/* 478 */	NdrFcShort( 0xfe5c ),	/* Offset= -420 (58) */
/* 480 */	0x8,		/* FC_LONG */
			0x8,		/* FC_LONG */
/* 482 */	0x5c,		/* FC_PAD */
			0x5b,		/* FC_END */
/* 484 */	
			0x1b,		/* FC_CARRAY */
			0x3,		/* 3 */
/* 486 */	NdrFcShort( 0x18 ),	/* 24 */
/* 488 */	0x27,		/* Corr desc:  parameter, FC_USHORT */
			0x0,		/*  */
/* 490 */	NdrFcShort( 0x4 ),	/* x86 Stack size/offset = 4 */
/* 492 */	NdrFcShort( 0x1 ),	/* Corr flags:  early, */
/* 494 */	0x4c,		/* FC_EMBEDDED_COMPLEX */
			0x0,		/* 0 */
/* 496 */	NdrFcShort( 0xffe8 ),	/* Offset= -24 (472) */
/* 498 */	0x5c,		/* FC_PAD */
			0x5b,		/* FC_END */
/* 500 */	
			0x11, 0x0,	/* FC_RP */
/* 502 */	NdrFcShort( 0x2 ),	/* Offset= 2 (504) */
/* 504 */	
			0x1b,		/* FC_CARRAY */
			0x3,		/* 3 */
/* 506 */	NdrFcShort( 0x4 ),	/* 4 */
/* 508 */	0x27,		/* Corr desc:  parameter, FC_USHORT */
			0x0,		/*  */
/* 510 */	NdrFcShort( 0x4 ),	/* x86 Stack size/offset = 4 */
/* 512 */	NdrFcShort( 0x1 ),	/* Corr flags:  early, */
/* 514 */	0x8,		/* FC_LONG */
			0x5b,		/* FC_END */
/* 516 */	
			0x11, 0x0,	/* FC_RP */
/* 518 */	NdrFcShort( 0x2 ),	/* Offset= 2 (520) */
/* 520 */	
			0x1b,		/* FC_CARRAY */
			0x3,		/* 3 */
/* 522 */	NdrFcShort( 0x10 ),	/* 16 */
/* 524 */	0x27,		/* Corr desc:  parameter, FC_USHORT */
			0x0,		/*  */
/* 526 */	NdrFcShort( 0x8 ),	/* x86 Stack size/offset = 8 */
/* 528 */	NdrFcShort( 0x1 ),	/* Corr flags:  early, */
/* 530 */	0x4c,		/* FC_EMBEDDED_COMPLEX */
			0x0,		/* 0 */
/* 532 */	NdrFcShort( 0xfe26 ),	/* Offset= -474 (58) */
/* 534 */	0x5c,		/* FC_PAD */
			0x5b,		/* FC_END */
/* 536 */	
			0x11, 0x0,	/* FC_RP */
/* 538 */	NdrFcShort( 0x2 ),	/* Offset= 2 (540) */
/* 540 */	
			0x1b,		/* FC_CARRAY */
			0x3,		/* 3 */
/* 542 */	NdrFcShort( 0x4 ),	/* 4 */
/* 544 */	0x27,		/* Corr desc:  parameter, FC_USHORT */
			0x0,		/*  */
/* 546 */	NdrFcShort( 0x8 ),	/* x86 Stack size/offset = 8 */
/* 548 */	NdrFcShort( 0x1 ),	/* Corr flags:  early, */
/* 550 */	0x8,		/* FC_LONG */
			0x5b,		/* FC_END */
/* 552 */	
			0x11, 0x0,	/* FC_RP */
/* 554 */	NdrFcShort( 0x2 ),	/* Offset= 2 (556) */
/* 556 */	
			0x1b,		/* FC_CARRAY */
			0x3,		/* 3 */
/* 558 */	NdrFcShort( 0x4 ),	/* 4 */
/* 560 */	0x27,		/* Corr desc:  parameter, FC_USHORT */
			0x0,		/*  */
/* 562 */	NdrFcShort( 0x8 ),	/* x86 Stack size/offset = 8 */
/* 564 */	NdrFcShort( 0x1 ),	/* Corr flags:  early, */
/* 566 */	
			0x4b,		/* FC_PP */
			0x5c,		/* FC_PAD */
/* 568 */	
			0x48,		/* FC_VARIABLE_REPEAT */
			0x49,		/* FC_FIXED_OFFSET */
/* 570 */	NdrFcShort( 0x4 ),	/* 4 */
/* 572 */	NdrFcShort( 0x0 ),	/* 0 */
/* 574 */	NdrFcShort( 0x1 ),	/* 1 */
/* 576 */	NdrFcShort( 0x0 ),	/* 0 */
/* 578 */	NdrFcShort( 0x0 ),	/* 0 */
/* 580 */	0x13, 0x0,	/* FC_OP */
/* 582 */	NdrFcShort( 0xfed4 ),	/* Offset= -300 (282) */
/* 584 */	
			0x5b,		/* FC_END */

			0x8,		/* FC_LONG */
/* 586 */	0x5c,		/* FC_PAD */
			0x5b,		/* FC_END */

			0x0
        }
    };

static void __RPC_USER IActivation_ORPC_EXTENTExprEval_0000( PMIDL_STUB_MESSAGE pStubMsg )
{
    ORPC_EXTENT *pS	=	( ORPC_EXTENT * )(( pStubMsg->StackTop - 20 ) );
    
    pStubMsg->Offset = 0;
    pStubMsg->MaxCount = ( unsigned long ) ( ( ( pS->size + 7 )  & ~7 )  );
}

static void __RPC_USER IActivation_ORPC_EXTENT_ARRAYExprEval_0001( PMIDL_STUB_MESSAGE pStubMsg )
{
    ORPC_EXTENT_ARRAY *pS	=	( ORPC_EXTENT_ARRAY * )pStubMsg->StackTop;
    
    pStubMsg->Offset = 0;
    pStubMsg->MaxCount = ( unsigned long ) ( ( ( pS->size + 1 )  & ~1 )  );
}

static const EXPR_EVAL ExprEvalRoutines[] = 
    {
    IActivation_ORPC_EXTENTExprEval_0000
    ,IActivation_ORPC_EXTENT_ARRAYExprEval_0001
    };



/* Standard interface: __MIDL_itf_objectexporter_0000_0000, ver. 0.0,
   GUID={0x00000000,0x0000,0x0000,{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}} */


/* Standard interface: IObjectExporter, ver. 0.0,
   GUID={0x99fcfec4,0x5260,0x101b,{0xbb,0xcb,0x00,0xaa,0x00,0x21,0x34,0x7a}} */


/* Standard interface: __MIDL_itf_objectexporter_0000_0001, ver. 0.0,
   GUID={0x00000000,0x0000,0x0000,{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}} */


/* Standard interface: IActivation, ver. 0.0,
   GUID={0x4d9f4ab8,0x7d1c,0x11cf,{0x86,0x1e,0x00,0x20,0xaf,0x6e,0x7c,0x57}} */


/* Standard interface: IRemoteSCMActivator, ver. 0.0,
   GUID={0x000001A0,0x0000,0x0000,{0xC0,0x00,0x00,0x00,0x00,0x00,0x00,0x46}} */


/* Standard interface: __MIDL_itf_objectexporter_0000_0003, ver. 0.0,
   GUID={0x00000000,0x0000,0x0000,{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}} */


/* Object interface: IUnknown, ver. 0.0,
   GUID={0x00000000,0x0000,0x0000,{0xC0,0x00,0x00,0x00,0x00,0x00,0x00,0x46}} */


/* Object interface: IRemUnknown, ver. 0.0,
   GUID={0x00000131,0x0000,0x0000,{0xC0,0x00,0x00,0x00,0x00,0x00,0x00,0x46}} */

#pragma code_seg(".orpc")
static const unsigned short IRemUnknown_FormatStringOffsetTable[] =
    {
    702,
    762,
    810
    };

static const MIDL_STUBLESS_PROXY_INFO IRemUnknown_ProxyInfo =
    {
    &Object_StubDesc,
    objectexporter__MIDL_ProcFormatString.Format,
    &IRemUnknown_FormatStringOffsetTable[-3],
    0,
    0,
    0
    };


static const MIDL_SERVER_INFO IRemUnknown_ServerInfo = 
    {
    &Object_StubDesc,
    0,
    objectexporter__MIDL_ProcFormatString.Format,
    &IRemUnknown_FormatStringOffsetTable[-3],
    0,
    0,
    0,
    0};
CINTERFACE_PROXY_VTABLE(6) _IRemUnknownProxyVtbl = 
{
    &IRemUnknown_ProxyInfo,
    &IID_IRemUnknown,
    IUnknown_QueryInterface_Proxy,
    IUnknown_AddRef_Proxy,
    IUnknown_Release_Proxy ,
    (void *) (INT_PTR) -1 /* IRemUnknown::RemQueryInterface */ ,
    (void *) (INT_PTR) -1 /* IRemUnknown::RemAddRef */ ,
    (void *) (INT_PTR) -1 /* IRemUnknown::RemRelease */
};

const CInterfaceStubVtbl _IRemUnknownStubVtbl =
{
    &IID_IRemUnknown,
    &IRemUnknown_ServerInfo,
    6,
    0, /* pure interpreted */
    CStdStubBuffer_METHODS
};


/* Object interface: IRemUnknown2, ver. 0.0,
   GUID={0x00000143,0x0000,0x0000,{0xC0,0x00,0x00,0x00,0x00,0x00,0x00,0x46}} */

#pragma code_seg(".orpc")
static const unsigned short IRemUnknown2_FormatStringOffsetTable[] =
    {
    702,
    762,
    810,
    852
    };

static const MIDL_STUBLESS_PROXY_INFO IRemUnknown2_ProxyInfo =
    {
    &Object_StubDesc,
    objectexporter__MIDL_ProcFormatString.Format,
    &IRemUnknown2_FormatStringOffsetTable[-3],
    0,
    0,
    0
    };


static const MIDL_SERVER_INFO IRemUnknown2_ServerInfo = 
    {
    &Object_StubDesc,
    0,
    objectexporter__MIDL_ProcFormatString.Format,
    &IRemUnknown2_FormatStringOffsetTable[-3],
    0,
    0,
    0,
    0};
CINTERFACE_PROXY_VTABLE(7) _IRemUnknown2ProxyVtbl = 
{
    &IRemUnknown2_ProxyInfo,
    &IID_IRemUnknown2,
    IUnknown_QueryInterface_Proxy,
    IUnknown_AddRef_Proxy,
    IUnknown_Release_Proxy ,
    (void *) (INT_PTR) -1 /* IRemUnknown::RemQueryInterface */ ,
    (void *) (INT_PTR) -1 /* IRemUnknown::RemAddRef */ ,
    (void *) (INT_PTR) -1 /* IRemUnknown::RemRelease */ ,
    (void *) (INT_PTR) -1 /* IRemUnknown2::RemQueryInterface2 */
};

const CInterfaceStubVtbl _IRemUnknown2StubVtbl =
{
    &IID_IRemUnknown2,
    &IRemUnknown2_ServerInfo,
    7,
    0, /* pure interpreted */
    CStdStubBuffer_METHODS
};


/* Standard interface: __MIDL_itf_objectexporter_0000_0005, ver. 0.0,
   GUID={0x00000000,0x0000,0x0000,{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}} */

static const MIDL_STUB_DESC Object_StubDesc = 
    {
    0,
    NdrOleAllocate,
    NdrOleFree,
    0,
    0,
    0,
    ExprEvalRoutines,
    0,
    objectexporter__MIDL_TypeFormatString.Format,
    1, /* -error bounds_check flag */
    0x50002, /* Ndr library version */
    0,
    0x800025b, /* MIDL Version 8.0.603 */
    0,
    0,
    0,  /* notify & notify_flag routine table */
    0x1, /* MIDL flag */
    0, /* cs routines */
    0,   /* proxy/server info */
    0
    };

const CInterfaceProxyVtbl * const _objectexporter_ProxyVtblList[] = 
{
    ( CInterfaceProxyVtbl *) &_IRemUnknownProxyVtbl,
    ( CInterfaceProxyVtbl *) &_IRemUnknown2ProxyVtbl,
    0
};

const CInterfaceStubVtbl * const _objectexporter_StubVtblList[] = 
{
    ( CInterfaceStubVtbl *) &_IRemUnknownStubVtbl,
    ( CInterfaceStubVtbl *) &_IRemUnknown2StubVtbl,
    0
};

PCInterfaceName const _objectexporter_InterfaceNamesList[] = 
{
    "IRemUnknown",
    "IRemUnknown2",
    0
};


#define _objectexporter_CHECK_IID(n)	IID_GENERIC_CHECK_IID( _objectexporter, pIID, n)

int __stdcall _objectexporter_IID_Lookup( const IID * pIID, int * pIndex )
{
    IID_BS_LOOKUP_SETUP

    IID_BS_LOOKUP_INITIAL_TEST( _objectexporter, 2, 1 )
    IID_BS_LOOKUP_RETURN_RESULT( _objectexporter, 2, *pIndex )
    
}

const ExtendedProxyFileInfo objectexporter_ProxyFileInfo = 
{
    (PCInterfaceProxyVtblList *) & _objectexporter_ProxyVtblList,
    (PCInterfaceStubVtblList *) & _objectexporter_StubVtblList,
    (const PCInterfaceName * ) & _objectexporter_InterfaceNamesList,
    0, /* no delegation */
    & _objectexporter_IID_Lookup, 
    2,
    2,
    0, /* table of [async_uuid] interfaces */
    0, /* Filler1 */
    0, /* Filler2 */
    0  /* Filler3 */
};
#pragma optimize("", on )
#if _MSC_VER >= 1200
#pragma warning(pop)
#endif


#endif /* !defined(_M_IA64) && !defined(_M_AMD64) && !defined(_ARM_) */

